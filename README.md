Start the project
1. git clone https://gitlab.com/DuongVu089x/crawler-server.git
2. cd crawler-server
3. run command: `go mod vendor`. If you don't have golang, let install it: https://golang.org/doc/install
4. run command: `go run main.go`
