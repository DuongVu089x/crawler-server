package core

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"runtime"
	"sync"
	"time"
)

// Error custom error of sdk
type Error struct {
	Type    string
	Message string
	Data    interface{}
}

func (e Error) ToError() error {
	return errors.New(e.Type + " : " + e.Message)
}

func (e *Error) Error() string {
	return e.Type + " : " + e.Message
}

// APIResponse This is response object with JSON format
type APIResponse struct {
	Status    string            `json:"status"`
	Data      interface{}       `json:"data,omitempty"`
	Message   string            `json:"message"`
	ErrorCode string            `json:"errorCode,omitempty"`
	Total     int64             `json:"total,omitempty"`
	Headers   map[string]string `json:"headers,omitempty"`
}

// statusEnum ...
type statusEnum struct {
	Ok           string
	Error        string
	Invalid      string
	NotFound     string
	Forbidden    string
	Existed      string
	Unauthorized string
}

// APIStatus Published enum
var APIStatus = &statusEnum{
	Ok:           "OK",
	Error:        "ERROR",
	Invalid:      "INVALID",
	NotFound:     "NOT_FOUND",
	Forbidden:    "FORBIDDEN",
	Existed:      "EXISTED",
	Unauthorized: "UNAUTHORIZED",
}

// methodValue ...
type methodValue struct {
	Value string
}

// methodEnum ...
type methodEnum struct {
	GET     *methodValue
	POST    *methodValue
	PUT     *methodValue
	DELETE  *methodValue
	OPTIONS *methodValue
}

// APIMethod Published enum
var APIMethod = methodEnum{
	GET:     &methodValue{Value: "GET"},
	POST:    &methodValue{Value: "POST"},
	PUT:     &methodValue{Value: "PUT"},
	DELETE:  &methodValue{Value: "DELETE"},
	OPTIONS: &methodValue{Value: "OPTIONS"},
}

type value struct {
	ZeroInt   int
	ZeroInt64 int64
	True      bool
	False     bool
	Empty     string
	None      string
}

var Values = value{
	ZeroInt:   0,
	ZeroInt64: 0,
	True:      true,
	False:     false,
	Empty:     "",
	None:      "NONE",
}

// BasicInfo Basic stored model
type BasicInfo struct {
	ID              primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	CreatedTime     time.Time          `json:"createdTime,omitempty"`
	LastUpdatedTime time.Time          `json:"lastUpdatedTime,omitempty"`
}

//App ..
type App struct {
	Name             string
	ServerList       []APIServer
	DBList           []*DBClient
	onAllDBConnected func()
	launched         bool
	hostname         string
}

// NewApp Wrap application
func NewApp(name string) *App {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "undefined"
	}
	app := &App{
		Name:       name,
		ServerList: []APIServer{},
		DBList:     []*DBClient{},
		launched:   false,
		hostname:   hostname,
	}
	return app
}

func (app *App) SetTimezone(loc *time.Location) error {
	if loc == nil {
		return Error{Type: "APP_CONFIG", Message: "Invalid location"}.ToError()
	}

	if app.launched {
		return Error{Type: "APP_CONFIG", Message: "Just allow to set timezone before launching app"}.ToError()
	}

	return nil
}

func (app *App) GetConfigFromEnv() (map[string]string, error) {
	var configMap map[string]string
	//src := []byte("{\"authDB\":\"admin\",\"dbHost\":\"10.42.4.165\",\"dbUser\":\"mongo_admin\",\"dbPassword\":\"ukxYzpGxFcSQxeN4fMMEpxCf7yzv6q\"}")
	//configStr := base64.URLEncoding.EncodeToString(src)

	configStr := os.Getenv("config")
	decoded, err := base64.URLEncoding.DecodeString(configStr)
	if err != nil {
		fmt.Println("[Parse config] Convert B64 config string error: " + err.Error())
		return nil, err
	}
	err = json.Unmarshal(decoded, &configMap)
	if err != nil {
		fmt.Println("[Parse config] Parse JSON with config string error: " + err.Error())
		return nil, err
	}
	return configMap, err
}

func (app *App) GetHostname() string {
	return app.hostname
}

// SetupDBClient ...
func (app *App) SetupDBClient(config DBConfiguration) *DBClient {
	if config.DatabaseName == "" {
		panic(Error{Type: "INVALID_CONFIG", Message: "Require database name"}.ToError())
	}
	dbname, _ := json.Marshal(config.Address)
	var db = &DBClient{
		Name:   string(dbname) + config.AuthDB + "@" + config.Username + ":" + config.Password[len(config.Password)-5:],
		Config: config,
	}
	app.DBList = append(app.DBList, db)
	return db
}

// SetupDBClient ...
func (app *App) OnAllDBConnected(task func()) {
	app.onAllDBConnected = task
}

// SetupAPIServer ...
func (app *App) SetupAPIServer(t string) (APIServer, error) {
	var newID = len(app.ServerList) + 1
	var server APIServer
	switch t {
	case "HTTP":
		server = newHTTPAPIServer(newID, app.hostname)
	}

	if server == nil {
		return nil, errors.New("server type " + t + " is invalid (HTTP/THRIFT)")
	}
	app.ServerList = append(app.ServerList, server)
	return server, nil
}

// callGCManually
func callGCManually() {
	for {
		time.Sleep(2 * time.Minute)
		runtime.GC()
	}
}

// Launch Launch app
func (app *App) Launch() error {

	if app.launched {
		return nil
	}

	app.launched = true

	name := app.Name + " / " + app.hostname
	fmt.Println("[ App " + name + " ] Launching ...")
	var wg = sync.WaitGroup{}

	// start connect to DB
	if len(app.DBList) > 0 {
		for _, db := range app.DBList {
			err := db.connect()
			if err != nil {
				fmt.Println("Connect DB " + db.Name + " error: " + err.Error())
				return err
			}
		}
		fmt.Println("[ App " + name + " ] DBs connected.")
	}

	// start servers
	if len(app.ServerList) > 0 {
		for _, s := range app.ServerList {
			wg.Add(1)
			go s.Start(&wg)
		}
		fmt.Println("[ App " + name + " ] API Servers started.")
	}

	if app.onAllDBConnected != nil {
		app.onAllDBConnected()
		fmt.Println("[ App " + name + " ] On-all-DBs-connected handler executed.")
	}

	fmt.Println("[ App " + name + " ] Totally launched!")
	go callGCManually()
	wg.Wait()

	return nil
}
