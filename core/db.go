package core

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	/* --- Field name --- */
	_lastUpdatedTime = "updated_time"

	/* --- DB connection config --- */
	_maxPoolSize     uint64 = 200
	_minPoolSize     uint64 = 3
	_maxConnIdleTime        = time.Duration(600000000) // 600s

	/* --- Options for execution --- */
	_defaultQLimit     = 10
	_maxQLimit         = 1000
	_returnDoc         = options.After
	optionReturnNewDoc = &options.FindOneAndUpdateOptions{
		ReturnDocument: &_returnDoc,
	}
	optionUpsertOne = &options.FindOneAndUpdateOptions{
		ReturnDocument: &_returnDoc,
		Upsert:         &Values.True,
	}
)

type (
	MgoProj              map[string]int
	OnDBConnectedHandler func(session *DBSession) error
)

type DBConfiguration struct {
	ConnectURL         string
	Address            []string
	Username           string
	Password           string
	AuthDB             string
	ReplicaSetName     string
	DatabaseName       string
	Ssl                bool
	SecondaryPreferred bool

	timezone *time.Location
}

type Document struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	CreatedTime time.Time          `json:"created_time,omitempty" bson:"created_time,omitempty"`
	UpdatedTime time.Time          `json:"updated_time,omitempty" bson:"updated_time,omitempty"`
}

type Increment struct {
	Value     int
	FieldName string
}

type DBClient struct {
	Name        string
	Config      DBConfiguration
	onConnected OnDBConnectedHandler
}

type DBSession struct {
	database *mongo.Database
	dbName   string
	auth     string
}

func (s *DBSession) GetDatabaseName() string {
	return s.dbName
}

func (s *DBSession) GetDatabase() *mongo.Database {
	return s.database
}

func (client *DBClient) OnConnected(fn OnDBConnectedHandler) {
	client.onConnected = fn
}

func (client *DBClient) connect() error {
	var dbClient *mongo.Client
	var err error
	if client.Config.ConnectURL == "" {
		mongoClient := &options.ClientOptions{
			Auth: &options.Credential{
				AuthSource: client.Config.AuthDB,
				Username:   client.Config.Username,
				Password:   client.Config.Password,
			},
			HeartbeatInterval: &_maxConnIdleTime,
			Hosts:             client.Config.Address,
			LocalThreshold:    nil,
			MaxConnIdleTime:   &_maxConnIdleTime,
			MaxPoolSize:       &_maxPoolSize,
			MinPoolSize:       &_minPoolSize,
			ReplicaSet:        &client.Config.ReplicaSetName,
			RetryReads:        &Values.False,
			RetryWrites:       &Values.False,
		}
		if client.Config.Ssl {
			mongoClient.TLSConfig = &tls.Config{
				InsecureSkipVerify:          false,
			}
		}
		dbClient, err = mongo.Connect(nil, mongoClient)
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		dbClient, err = mongo.Connect(ctx, options.Client().ApplyURI(client.Config.ConnectURL))
	}
	if err != nil {
		return err
	}

	err = dbClient.Ping(nil, nil)
	if err != nil {
		return err
	}

	err = client.onConnected(&DBSession{
		database: dbClient.Database(client.Config.DatabaseName, nil),
		dbName:   client.Config.DatabaseName,
		auth:     client.Config.AuthDB,
	})
	return err
}

type DBModel struct {
	CollectionName string
	TemplateObject interface{}

	mSession   *DBSession
	collection *mongo.Collection
}

func (m *DBModel) convertToObject(b bson.M) (interface{}, error) {
	t := reflect.TypeOf(m.TemplateObject)
	v := reflect.New(t)
	obj := v.Interface()

	if b == nil {
		return obj, nil
	}

	bytes, err := bson.Marshal(b)
	if err != nil {
		return nil, err
	}

	err = bson.Unmarshal(bytes, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

func (m *DBModel) newList(limit int) interface{} {
	t := reflect.TypeOf(m.TemplateObject)
	return reflect.MakeSlice(reflect.SliceOf(t), 0, limit).Interface()
}

func (m *DBModel) newListWithType(limit int, t reflect.Type) interface{} {
	return reflect.MakeSlice(reflect.SliceOf(t), 0, limit).Interface()
}

func (m *DBModel) Init(session *DBSession) error {
	if len(m.CollectionName) == 0 {
		return Error{Type: "INVALID_CONFIG", Message: "Require collection name"}.ToError()
	}
	if m.TemplateObject == nil {
		return Error{Type: "INVALID_CONFIG", Message: "Require template object"}.ToError()
	}
	if session == nil {
		return Error{Type: "INVALID_CONFIG", Message: "Invalid session"}.ToError()
	}

	m.mSession = session
	m.collection = session.database.Collection(m.CollectionName)
	return nil
}

func (m *DBModel) GetDatabaseName() string {
	return m.mSession.dbName
}

func (m *DBModel) GetCollectionName() string {
	return m.CollectionName
}

func (m *DBModel) GetNamespace() string {
	return fmt.Sprintf("%s.%s", m.mSession.dbName, m.CollectionName)
}

func (m *DBModel) GetSession() *DBSession {
	c := m.mSession.database.Client()
	newClient := *c
	return &DBSession{
		database: newClient.Database(m.mSession.dbName, nil),
		dbName:   m.mSession.dbName,
		auth:     m.mSession.auth,
	}
}

func (m *DBModel) GetIndexes() ([]bson.M, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := m.collection.Indexes().List(ctx)
	if err != nil {
		return nil, err
	}

	if cursor.Err() != nil {
		return nil, cursor.Err()
	}

	var listIndexes []bson.M
	err = cursor.All(ctx, &listIndexes)
	if err != nil {
		return nil, err
	}

	return listIndexes, nil
}

func (m *DBModel) CreateIndex(index mongo.IndexModel, opts *options.CreateIndexesOptions) error {
	indexName, err := m.collection.Indexes().CreateOne(context.Background(), index, opts)

	if indexName != "" {
		println(fmt.Sprintf("Created index '%s' on %s successfully", indexName, m.CollectionName))
	}
	return err
}

func (m *DBModel) CreateOne(ctx context.Context, object interface{}) *APIResponse {
	if object == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	return m.Create(ctx, []interface{}{object})
}

func (m *DBModel) Create(ctx context.Context, objects []interface{}) *APIResponse {
	out, err := m.c(ctx, objects)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   err.Message,
			ErrorCode: err.Type,
		}
	}

	list := m.newList(len(out))
	listValue := reflect.ValueOf(list)
	for i := range out {
		obj, err2 := m.convertToObject(out[i])
		if err2 != nil {
			println(err2.Error())
		}
		listValue = reflect.Append(listValue, reflect.Indirect(reflect.ValueOf(obj)))
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Data:    listValue.Interface(),
		Message: "Create " + m.CollectionName + " successfully",
	}
}

func (m *DBModel) c(ctx context.Context, objects []interface{}) ([]bson.M, *Error) {
	if objects == nil || len(objects) == 0 {
		return nil, &Error{
			Message: "Object error: empty list",
			Type:    "EMPTY_LIST",
		}
	}

	newObjs := make([]interface{}, 0, len(objects))
	for i := range objects {
		newObj, err := ObjectToBson(objects[i])
		if err != nil {
			return nil, &Error{
				Message: "Object error: " + err.Error(),
				Type:    "MAP_OBJECT_FAILED",
			}
		}

		if newObj["created_time"] == nil {
			newObj["created_time"] = time.Now()
			newObj[_lastUpdatedTime] = newObj["created_time"]
		} else {
			newObj[_lastUpdatedTime] = time.Now()
		}

		newObjs = append(newObjs, newObj)
	}

	if ctx == nil {
		ctx = context.Background()
	}

	res, err := m.collection.InsertMany(ctx, newObjs)
	if err != nil {
		writeEx, ok := err.(mongo.WriteException)
		if ok {
			if writeEx.WriteErrors[0].Code == 11000 {
				return nil, &Error{
					Message: err.Error(),
					Type:    "DUPLICATE_KEY_ERROR",
				}
			}
		}

		return nil, &Error{
			Message: "DB error: " + err.Error(),
			Type:    "DB_ERROR",
		}
	}

	if res.InsertedIDs == nil || len(res.InsertedIDs) < 1 {
		return nil, &Error{
			Message: "DB error: Nothing new",
			Type:    "DB_ERROR",
		}
	}

	qRes, _ := m.collection.Find(ctx, bson.M{"_id": bson.M{"$in": res.InsertedIDs}})
	if qRes.Err() != nil {
		return nil, &Error{
			Message: "DB error: " + qRes.Err().Error(),
			Type:    "DB_ERROR",
		}
	}

	var out []bson.M
	err = qRes.All(ctx, &out)
	if err != nil {
		return nil, &Error{
			Message: "DB error: " + err.Error(),
			Type:    "DB_ERROR",
		}
	}

	if len(out) == 0 {
		return nil, &Error{
			Message: "DB error: len(out) = 0",
			Type:    "DB_ERROR",
		}
	}

	return out, nil
}

func (m *DBModel) DeleteOne(ctx context.Context, filter interface{}) *APIResponse {
	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	dRes, err := m.collection.DeleteOne(ctx, filter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Delete error: " + err.Error(),
			ErrorCode: "DELETE_FAILED",
		}
	}

	if dRes.DeletedCount == 0 {
		return &APIResponse{
			Status:  APIStatus.NotFound,
			Message: "Not found any matched in " + m.CollectionName,
			Total:   0,
		}
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Delete " + m.CollectionName + " successfully",
		Total:   dRes.DeletedCount,
	}
}

func (m *DBModel) DeleteMany(ctx context.Context, filter interface{}) *APIResponse {
	if filter == nil {
		return &APIResponse{
			Status:    APIStatus.Invalid,
			Message:   "Risk detected: clear all data",
			ErrorCode: "UNACCEPTED_COMMAND",
		}
	} else {
		// TODO: handle in cases bson.M{} and empty map
	}

	if ctx == nil {
		ctx = context.Background()
	}
	dRes, err := m.collection.DeleteOne(ctx, filter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Delete error: " + err.Error(),
			ErrorCode: "DELETE_FAILED",
		}
	}

	if dRes.DeletedCount == 0 {
		return &APIResponse{
			Status:  APIStatus.NotFound,
			Message: "Not found any matched in " + m.CollectionName,
			Total:   0,
		}
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Delete " + m.CollectionName + " successfully",
		Total:   dRes.DeletedCount,
	}
}

func (m *DBModel) QueryOne(ctx context.Context, filter interface{}, projection MgoProj) *APIResponse {
	return m.Q(ctx, filter, nil, projection, 0, 1)
}

func (m *DBModel) Query(ctx context.Context, filter interface{}, offset int, limit int) *APIResponse {
	return m.Q(ctx, filter, nil, nil, offset, limit)
}

func (m *DBModel) Q(ctx context.Context, filter interface{}, sortFields []string, projection MgoProj, offset int, limit int) *APIResponse {
	out, err := m.q(ctx, filter, sortFields, projection, offset, limit)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "QUERY_FAILED",
		}
	} else if len(out) == 0 {
		return &APIResponse{
			Status:  APIStatus.NotFound,
			Message: "Not found any matched in " + m.CollectionName,
		}
	} else {
		list := m.newList(limit)
		listValue := reflect.ValueOf(list)

		for i := range out {
			obj, err := m.convertToObject(out[i])
			if err != nil {
				println(err.Error())
			}
			listValue = reflect.Append(listValue, reflect.Indirect(reflect.ValueOf(obj)))
		}

		return &APIResponse{
			Status:  APIStatus.Ok,
			Data:    listValue.Interface(),
			Message: "Query " + m.CollectionName + " successfully",
		}
	}
}

func (m *DBModel) q(ctx context.Context, filter interface{}, sortFields []string, projection MgoProj, offset int, limit int) ([]bson.M, error) {
	o := int64(offset)
	if limit < 1 {
		limit = _defaultQLimit
		//} else if limit > _maxQLimit {
		//	limit = _maxQLimit
	}
	l := int64(limit)
	option := &options.FindOptions{
		Skip:  &o,
		Limit: &l,
	}

	if filter == nil {
		filter = bson.M{}
	}

	if sortFields != nil && len(sortFields) > 0 {
		sFields := bson.M{}
		for _, str := range sortFields {
			if str == "" {
				continue
			} else {
				switch str[0] {
				case '+':
					str = str[1:]
					sFields[str] = 1
				case '-':
					str = str[1:]
					sFields[str] = -1
				}
			}
		}
		option.Sort = sFields
	}

	if projection != nil {
		option.Projection = projection
	}

	if ctx == nil {
		ctx = context.Background()
	}

	qRes, err := m.collection.Find(ctx, filter, option)
	if err != nil {
		return nil, err
	}

	if qRes.Err() != nil {
		return nil, qRes.Err()
	}

	var out []bson.M
	err = qRes.All(ctx, &out)
	if err != nil {
		return nil, err
	} else if len(out) == 0 {
		return nil, nil
	} else {
		return out, nil
	}
}

func (m *DBModel) UpdateOne(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	return m.UpdateOneWithSorted(ctx, filter, updater, nil)
}

func (m *DBModel) UpdateOneWithSorted(ctx context.Context, filter interface{}, updater interface{}, sortFields []string) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}
	obj[_lastUpdatedTime] = time.Now()
	setter := bson.M{
		"$set": obj,
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	var uRes *mongo.SingleResult
	if sortFields == nil || len(sortFields) < 1 {
		uRes = m.collection.FindOneAndUpdate(ctx, filter, setter, optionReturnNewDoc)
	} else {
		option := *optionReturnNewDoc
		if sortFields != nil && len(sortFields) > 0 {
			sFields := bson.M{}
			for _, str := range sortFields {
				if str == "" {
					continue
				} else {
					switch str[0] {
					case '+':
						str = str[1:]
						sFields[str] = 1
					case '-':
						str = str[1:]
						sFields[str] = -1
					}
				}
			}
			option.Sort = sFields
		}
		uRes = m.collection.FindOneAndUpdate(ctx, filter, setter, &option)
	}

	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Update " + m.CollectionName + " successfully",
	}

	var out bson.M
	err = uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) Update(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}
	obj[_lastUpdatedTime] = time.Now()
	setter := bson.M{
		"$set": obj,
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes, err := m.collection.UpdateMany(ctx, filter, setter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + err.Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	if uRes.MatchedCount == uRes.ModifiedCount {
		return &APIResponse{
			Status:  APIStatus.Ok,
			Message: "Update " + m.CollectionName + " successfully",
			Total:   uRes.ModifiedCount,
		}
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Update " + m.CollectionName + " successfully " + fmt.Sprintf("%d/%d", uRes.ModifiedCount, uRes.MatchedCount),
		Total:   uRes.ModifiedCount,
	}
}

func (m *DBModel) UpsertOne(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}

	// Handle while object has been filled created_time
	obj[_lastUpdatedTime] = time.Now()
	createdTime, ok := obj["created_time"]
	if !ok || createdTime == nil {
		createdTime = obj[_lastUpdatedTime]
	}

	setter := bson.M{
		"$set": obj,
		"$setOnInsert": bson.M{
			"created_time": createdTime,
		},
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes := m.collection.FindOneAndUpdate(ctx, filter, setter, optionUpsertOne)
	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Upsert " + m.CollectionName + " successfully",
	}

	var out bson.M
	err = uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) IncreaseOne(ctx context.Context, filter interface{}, inc Increment) *APIResponse {
	if filter == nil {
		filter = bson.M{}
	}

	setter := bson.M{
		"$inc": bson.M{
			inc.FieldName: inc.Value,
		},
		"$currentDate": bson.M{
			_lastUpdatedTime: true,
		},
		"$setOnInsert": bson.M{
			"created_time": time.Now(),
		},
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes := m.collection.FindOneAndUpdate(ctx, filter, setter, optionUpsertOne)
	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Update " + m.CollectionName + " successfully",
	}

	var out bson.M
	err := uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) ReplaceOne(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}
	obj[_lastUpdatedTime] = time.Now()
	setter := bson.M{
		"$set": obj,
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes := m.collection.FindOneAndReplace(ctx, filter, setter)
	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Upsert " + m.CollectionName + " successfully",
	}

	var out bson.M
	err = uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		m.collection.FindOne(ctx, bson.M{})

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) PullOne(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}
	setter := bson.M{
		"$pull": obj,
		"$currentDate": bson.M{
			_lastUpdatedTime: true,
		},
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes := m.collection.FindOneAndUpdate(ctx, filter, setter, optionReturnNewDoc)
	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Update " + m.CollectionName + " successfully",
	}

	var out bson.M
	err = uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) PushOne(ctx context.Context, filter interface{}, updater interface{}) *APIResponse {
	if updater == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Object error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	obj, err := ObjectToBson(updater)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "DB Error: " + err.Error(),
			ErrorCode: "MAP_OBJECT_FAILED",
		}
	}
	setter := bson.M{
		"$push": obj,
		"$currentDate": bson.M{
			_lastUpdatedTime: true,
		},
	}

	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	uRes := m.collection.FindOneAndUpdate(ctx, filter, setter, optionReturnNewDoc)
	if uRes.Err() != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Update error: " + uRes.Err().Error(),
			ErrorCode: "UPDATE_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Update " + m.CollectionName + " successfully",
	}

	var out bson.M
	err = uRes.Decode(&out)
	if err != nil {
		println(err.Error())
	} else {
		obj, err := m.convertToObject(out)
		if err != nil {
			println(err.Error())
		}

		list := m.newList(1)
		listValue := reflect.Append(
			reflect.ValueOf(list),
			reflect.Indirect(reflect.ValueOf(obj)),
		)

		response.Data = listValue.Interface()
	}

	return response
}

func (m *DBModel) Count(ctx context.Context, filter interface{}) *APIResponse {
	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	total, err := m.collection.CountDocuments(ctx, filter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Count error: " + err.Error(),
			ErrorCode: "COUNT_FAILED",
		}
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Count " + m.CollectionName + " successfully",
		Total:   total,
	}
}

func (m *DBModel) D(ctx context.Context, filter interface{}, fieldName string, normalize bool) *APIResponse {
	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	dRes, err := m.collection.Distinct(ctx, fieldName, filter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Distinct error: " + err.Error(),
			ErrorCode: "DISTINCT_FAILED",
		}
	}

	response := &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Distinct " + m.CollectionName + " successfully",
		Data:    dRes,
	}

	if !normalize {
		return response
	}

	//defer func() *APIResponse {
	//	// Handle when output is a slice of multi data type
	//	// Example: [ 1, 2.0, ... ]
	//	if err := recover(); err != nil {
	//		response.Message += " but failed to normalize output"
	//		return response
	//	}
	//
	//	return response
	//}()

	total := len(dRes)
	if dRes == nil || total == 0 {
		return nil
	}

	// Try to format data
	_t := reflect.TypeOf(dRes[0])
	list := m.newListWithType(total, _t)
	listValue := reflect.Append(
		reflect.ValueOf(list),
		reflect.Indirect(reflect.ValueOf(dRes[0])),
	)

	for _, i := range dRes[1:] {
		if reflect.TypeOf(i).Kind().String() == _t.Kind().String() {
			listValue = reflect.Append(
				reflect.ValueOf(list),
				reflect.Indirect(reflect.ValueOf(i)),
			)
		} else {
			response.Message += " but failed to normalize output"
			return response
		}
	}

	response.Data = listValue.Interface()
	return nil
}

func (m *DBModel) Distinct(ctx context.Context, filter interface{}, fieldName string, templateObject interface{}) *APIResponse {
	if filter == nil {
		filter = bson.M{}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	dRes, err := m.collection.Distinct(ctx, fieldName, filter)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Distinct error: " + err.Error(),
			ErrorCode: "DISTINCT_FAILED",
		}
	}

	b, err := json.Marshal(dRes)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Distinct error: " + err.Error(),
			ErrorCode: "DISTINCT_FAILED",
		}
	}

	err = json.Unmarshal(b, templateObject)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Distinct error: " + err.Error(),
			ErrorCode: "DISTINCT_FAILED",
		}
	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Data:    templateObject,
		Message: "Distinct " + m.CollectionName + " successfully",
	}
}

func (m *DBModel) Aggregate(ctx context.Context, pipeline interface{}) *APIResponse {
	if pipeline == nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Pipeline error: null pointer",
			ErrorCode: "NULL_POINTER",
		}
	}

	if ctx == nil {
		ctx = context.Background()
	}

	aRes, err := m.collection.Aggregate(ctx, pipeline)
	if err != nil {
		return &APIResponse{
			Status:    APIStatus.Error,
			Message:   "Aggregate error: " + err.Error(),
			ErrorCode: "AGGREGATE_FAILED",
		}
	}

	var out []bson.M
	err = aRes.All(ctx, &out)
	if err != nil {
		println(err.Error())
	} else {

	}

	return &APIResponse{
		Status:  APIStatus.Ok,
		Message: "Aggregate " + m.CollectionName + " successfully",
		Data:    out,
	}
}
