package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/DuongVu089x/crawler-server/api"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

var apiInfo APIInfo

func main() {

	//e := echo.New()
	//e.GET("/", api.Hello)

	//dbName = "crawler_dev"

	// init DB
	//initDB()
	//var mongoURI = "mongodb+srv://crawler:OXcaZPIPpn7CKSf8@cluster0.i2iox.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()
	//client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	//if err != nil {
	//	log.Fatal(err)
	//	return nil, err
	//}
	//
	//log.Printf("Connect success")
	//return client, nil

	apiInfo = APIInfo{
		Version:   os.Getenv("version"),
		Env:       "dev",
		StartTime: time.Now(),
	}

	// setup new app
	var app = core.NewApp("Crawler App")
	db := app.SetupDBClient(core.DBConfiguration{
		//Address: []string{
		//	"cluster0-shard-00-00.i2iox.mongodb.net:27017",
		//	"cluster0-shard-00-01.i2iox.mongodb.net:27017",
		//	"cluster0-shard-00-02.i2iox.mongodb.net:27017",
		//},
		Username: "crawler",
		Password: "OXcaZPIPpn7CKSf8",
		//AuthDB:             "admin",
		//ReplicaSetName:     "atlas-ru6yq6-shard-0",
		DatabaseName: "crawler_dev",
		//Ssl:                true,
		//SecondaryPreferred: false,
		ConnectURL: "mongodb+srv://crawler:OXcaZPIPpn7CKSf8@cluster0.i2iox.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	})
	log.Print(db)
	db.OnConnected(onDBConnected)

	//// START Demo insert data
	//now := time.Now()
	//name := fmt.Sprint("pi", +now.Nanosecond())
	//coinDemo := model.CoinModel{
	//	Name: name,
	//}
	//model.CoinDB.Insert(coinDemo)
	//// END Demo insert data

	//e.Logger.Fatal(e.Start(":1323"))

	// setup API Server
	var server, _ = app.SetupAPIServer("HTTP")

	server.SetHandler(core.APIMethod.GET, "/crawl-server/api-info", info)
	server.SetHandler(core.APIMethod.GET, "/crawl-server/block", api.ListBlocksGET)
	server.SetHandler(core.APIMethod.GET, "/crawl-server/coin", api.ListCoinsGET)
	server.SetHandler(core.APIMethod.GET, "/crawl-server/coin/batch", api.GetCoinInfoDayGET)
	server.SetHandler(core.APIMethod.GET, "/crawl-server/trade", api.ListTradeByCoinGET)

	server.SetHandler(core.APIMethod.POST, "/crawl-server/user/register", api.AccountRegisterPOST)
	server.SetHandler(core.APIMethod.GET, "/crawl-server/user/check-email", api.CheckEmailGET)

	server.Expose(80)

	app.OnAllDBConnected(onAllDBConnected)

	// launch app
	err := app.Launch()

	if err != nil {
		name, _ := os.Hostname()
		fmt.Println("Launch error " + name + " " + err.Error())
	}
}

func onDBConnected(session *core.DBSession) error {
	model.InitCoinDB(session)
	model.InitBlockDB(session)
	model.InitTradeDB(session)
	model.InitAccountDB(session)
	return nil
}

func onAllDBConnected() {
	//action.GetEthPriceDay()
	// go action.GetCoinInfoDay()
}

// APIInfo ...
type APIInfo struct {
	StartTime time.Time
	Env       string
	Version   string
}

func info(req core.APIRequest, res core.APIResponder) error {
	return res.Respond(&core.APIResponse{
		Status: core.APIStatus.Ok,
		Data:   []APIInfo{apiInfo},
	})
}
