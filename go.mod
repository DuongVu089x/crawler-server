module gitlab.com/DuongVu089x/crawler-server

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/gocolly/colly v1.2.0
	github.com/gocolly/colly/v2 v2.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.17
	go.mongodb.org/mongo-driver v1.3.1
)
