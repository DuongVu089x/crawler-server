package model

import (
	"fmt"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Coin struct {
	Code      string  `json:"code,omitempty" bson:"code,omitempty"`
	Name      string  `json:"name,omitempty" bson:"name,omitempty"`
	PriceUsd  float64 `json:"priceUsd,omitempty" bson:"price_usd,omitempty"`
	PriceBtc  float64 `json:"priceBtc,omitempty" bson:"price_btc,omitempty"`
	PriceEth  float64 `json:"priceEth,omitempty" bson:"price_eth,omitempty"`
	Volume    float64 `json:"volume,omitempty" bson:"volume,omitempty"`
	Change    float64 `json:"change,omitempty" bson:"change,omitempty"`
	MarketCap float64 `json:"marketCap,omitempty" bson:"market_cap,omitempty"`
	Holders   float64 `json:"holders,omitempty" bson:"holders,omitempty"`

	Version string `json:"version,omitempty" bson:"version,omitempty"`

	// Basic Info
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	CreatedTime     *time.Time         `bson:"created_time,omitempty" json:"createdTime,omitempty"`
	LastUpdatedTime *time.Time         `bson:"last_updated_time,omitempty" json:"lastUpdatedTime,omitempty"`
}

var CoinDB = &core.DBModel{
	CollectionName: "coin",
	TemplateObject: &Coin{},
	//core             *mongo.Database
}

func InitCoinDB(session *core.DBSession) {

	err := CoinDB.Init(session)
	if err != nil {
		panic(err)
	}

	err = CoinDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"code":    1,
			"version": 1,
		},
		Options: &options.IndexOptions{
			Unique:     &core.Values.True,
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

}
