package model

import (
	"fmt"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Block struct {
	Version     string     `json:"version,omitempty" bson:"version,omitempty"`
	BlockCode   string     `json:"blockCode,omitempty" bson:"block_code,omitempty"`
	Age         *time.Time `json:"age,omitempty" bson:"age,omitempty"`
	Miner       string     `json:"miner,omitempty" bson:"miner,omitempty"`
	GasUsed     string     `json:"gasUsed,omitempty" bson:"gas_used,omitempty"`
	GasLimit    string     `json:"gasLimit,omitempty" bson:"gas_limit,omitempty"`
	AvgGasPrice string     `json:"avgGasPrice,omitempty" bson:"avg_gas_price,omitempty"`
	Reward      string     `json:"reward,omitempty" bson:"reward,omitempty"`

	// Basic Info
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	CreatedTime     *time.Time         `bson:"created_time,omitempty" json:"createdTime,omitempty"`
	LastUpdatedTime *time.Time         `bson:"last_updated_time,omitempty" json:"lastUpdatedTime,omitempty"`
}

var BlockDB = &core.DBModel{
	CollectionName: "block",
	TemplateObject: &Block{},
}

func InitBlockDB(session *core.DBSession) {

	err := BlockDB.Init(session)
	if err != nil {
		panic(err)
	}

	err = BlockDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"name":    1,
			"version": 1,
		},
		Options: &options.IndexOptions{
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

}
