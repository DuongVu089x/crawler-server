package model

import (
	"fmt"
	"time"

	"gitlab.com/DuongVu089x/crawler-server/core"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Account struct {
	UserName         string `json:"userName,omitempty" bson:"user_name,omitempty"`
	FirstName        string `json:"firstName,omitempty" bson:"first_name,omitempty"`
	LastName         string `json:"lastName,omitempty" bson:"last_name,omitempty"`
	Email            string `json:"email,omitempty" bson:"email,omitempty"`
	Password         string `json:"password,omitempty" bson:"password,omitempty"`
	AllowExtraEmails bool   `json:"allowExtraEmails,omitempty" bson:"allow_extra_emails,omitempty"`

	// Basic Info
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	CreatedTime     *time.Time         `bson:"created_time,omitempty" json:"createdTime,omitempty"`
	LastUpdatedTime *time.Time         `bson:"last_updated_time,omitempty" json:"lastUpdatedTime,omitempty"`
}

var AccountDB = &core.DBModel{
	CollectionName: "account",
	TemplateObject: &Account{},
}

func InitAccountDB(session *core.DBSession) {

	err := AccountDB.Init(session)
	if err != nil {
		panic(err)
	}

	err = AccountDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"user_name": 1,
		},
		Options: &options.IndexOptions{
			Unique:     &core.Values.True,
			Sparse:     &core.Values.True,
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

	err = AccountDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"email": 1,
		},
		Options: &options.IndexOptions{
			Unique:     &core.Values.True,
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

}
