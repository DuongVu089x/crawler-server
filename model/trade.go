package model

import (
	"fmt"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Trade struct {
	CoinCode       string     `json:"coinCode,omitempty" bson:"coin_code,omitempty"`
	TxnHash        string     `json:"txnHash,omitempty" bson:"txn_hash,omitempty"`
	Age            *time.Time `json:"age,omitempty" bson:"age,omitempty"`
	Action         string     `json:"action,omitempty" bson:"action,omitempty"`
	TokenAmountOut float64    `json:"tokenAmountOut,omitempty" bson:"token_amount_out,omitempty"`
	TokenAmountIn  float64    `json:"tokenAmountIn,omitempty" bson:"token_amount_in,omitempty"`
	SwappedRate    float64    `json:"swappedRate,omitempty" bson:"swapped_rate,omitempty"`
	TxnValue       float64    `json:"txnValue,omitempty" bson:"txn_value,omitempty"`

	Version string `json:"version,omitempty" bson:"version,omitempty"`

	// Basic Info
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	CreatedTime     *time.Time         `bson:"created_time,omitempty" json:"createdTime,omitempty"`
	LastUpdatedTime *time.Time         `bson:"last_updated_time,omitempty" json:"lastUpdatedTime,omitempty"`
}

var TradeDB = &core.DBModel{
	CollectionName: "trade",
	TemplateObject: &Trade{},
	//core             *mongo.Database
}

func InitTradeDB(session *core.DBSession) {

	err := TradeDB.Init(session)
	if err != nil {
		panic(err)
	}

	err = TradeDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"version":   1,
			"coin_code": 1,
			"txn_hash":  1,
		},
		Options: &options.IndexOptions{
			Unique:     &core.Values.True,
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

	err = TradeDB.CreateIndex(mongo.IndexModel{
		Keys: bson.M{
			"version":   1,
			"coin_code": 1,
		},
		Options: &options.IndexOptions{
			Background: &Background,
		},
	}, nil)

	if err != nil {
		fmt.Println("Create Index DB error: " + err.Error())
	}

}
