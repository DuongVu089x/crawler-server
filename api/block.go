package api

import (
	"encoding/json"

	"gitlab.com/DuongVu089x/crawler-server/action"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

func ListBlocksGET(req core.APIRequest, res core.APIResponder) error {

	var qStr = req.GetParam("q")
	var qInput model.Block
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &qInput)
		if err != nil {
			return res.Respond(&core.APIResponse{
				Status:    core.APIStatus.Invalid,
				Message:   "Cannot parse JSON input.",
				ErrorCode: "INVALID_INPUT",
			})
		}
	}

	var offset = action.ParseInt(req.GetParam("offset"), 0)
	var limit = action.ParseInt(req.GetParam("limit"), 10)
	var getTotal = req.GetParam("getTotal") == "true"

	return res.Respond(action.ListBlocks(&qInput, offset, limit, getTotal))
}
