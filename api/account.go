package api

import (
	"gitlab.com/DuongVu089x/crawler-server/action"
	"gitlab.com/DuongVu089x/crawler-server/core"
)

func AccountRegisterPOST(req core.APIRequest, res core.APIResponder) error {

	var content action.AccountRegisterInput
	err := req.GetContent(&content)
	if err != nil {
		return res.Respond(&core.APIResponse{Status: core.APIStatus.Invalid, Message: "Invalid input. Please put in JSON format." + err.Error()})
	}

	return res.Respond(action.AccountRegister(&content))
}

func CheckEmailGET(req core.APIRequest, res core.APIResponder) error {
	var email = req.GetParam("email")

	return res.Respond(action.CheckEmail(email))
}
