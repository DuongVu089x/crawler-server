package api

import (
	"encoding/json"

	"gitlab.com/DuongVu089x/crawler-server/action"
	"gitlab.com/DuongVu089x/crawler-server/core"
)

func ListCoinsGET(req core.APIRequest, res core.APIResponder) error {

	var qStr = req.GetParam("q")
	var qInput action.ListCoinInput
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &qInput)
		if err != nil {
			return res.Respond(&core.APIResponse{
				Status:    core.APIStatus.Invalid,
				Message:   "Cannot parse JSON input.",
				ErrorCode: "INVALID_INPUT",
			})
		}
	}

	var offset = action.ParseInt(req.GetParam("offset"), 0)
	var limit = action.ParseInt(req.GetParam("limit"), 10)
	var getTotal = req.GetParam("getTotal") == "true"

	return res.Respond(action.ListCoins(&qInput, offset, limit, getTotal))
}

func GetCoinInfoDayGET(req core.APIRequest, res core.APIResponder) error {
	go action.GetCoinInfoDay()
	return res.Respond(&core.APIResponse{Status: core.APIStatus.Ok, Message: "Success"})
}
