package action

import (
	"context"

	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

func ListBlocks(input *model.Block, offset, limit int, getTotal bool) *core.APIResponse {

	blockQuery := input
	blockQuery.Version = GetCurrentVersion()
	blockResp := model.BlockDB.Query(context.Background(), blockQuery, offset, limit)
	if blockResp.Status == core.APIStatus.Ok && getTotal {
		totalResp := model.BlockDB.Count(context.Background(), blockQuery)
		if totalResp.Status == core.APIStatus.Ok {
			blockResp.Total = totalResp.Total
		}
	}
	return blockResp
}
