package action

import (
	"context"

	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

type ListTradeByCoinInput struct {
	Trade     *model.Trade `json:"trade,omitempty"`
	SortField []string     `json:"sortField,omitempty"`
}

func ListTradeByCoin(input *ListTradeByCoinInput, offset, limit int, getTotal bool) *core.APIResponse {

	tradeQuery := input.Trade
	tradeQuery.Version = GetCurrentVersion()

	tradeResp := model.TradeDB.Q(context.Background(), tradeQuery, input.SortField, nil, offset, limit)
	if tradeResp.Status == core.APIStatus.Ok && getTotal {
		totalResp := model.TradeDB.Count(context.Background(), tradeQuery)
		if totalResp.Status == core.APIStatus.Ok {
			tradeResp.Total = totalResp.Total
		}
	}
	return tradeResp
}
