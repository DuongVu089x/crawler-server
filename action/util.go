package action

import (
	"strconv"
	"strings"
	"time"
)

func GetCurrentVersion() string {
	t := time.Now()
	// parse to time zone +7
	tz := time.FixedZone("UTC+7", +7*60*60)
	ictTime := t.In(tz)
	return ictTime.Format("20060102")
}

// ParseInt convert string to int
func ParseInt(text string, defaultValue int) int {
	if text == "" {
		return defaultValue
	}

	num, err := strconv.Atoi(text)
	if err != nil {
		return defaultValue
	}
	return num
}

// ParseFloat convert string to int
func ParseFloat(text string, defaultValue float64) float64 {
	text = strings.TrimSpace(text)
	text = strings.ReplaceAll(text, ",", "")

	if text == "" {
		return defaultValue
	}

	num, err := strconv.ParseFloat(text, 64)
	if err != nil {
		return defaultValue
	}
	return num
}
