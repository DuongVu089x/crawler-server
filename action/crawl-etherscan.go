package action

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	"github.com/gocolly/colly"
	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
	"go.mongodb.org/mongo-driver/bson"
)

func GetEthPriceDay() *core.APIResponse {

	var coin *model.Coin

	version := GetCurrentVersion()
	coinQuery := model.Coin{
		Version: version,
		Name:    "ETH",
	}
	coinResp := model.CoinDB.QueryOne(nil, coinQuery, nil)
	if coinResp.Status == core.APIStatus.Error {
		return coinResp
	}

	if coinResp.Status == core.APIStatus.NotFound {
		model.CoinDB.CreateOne(nil, coinQuery)
		coinResp := model.CoinDB.QueryOne(nil, coinQuery, nil)
		if coinResp.Status == core.APIStatus.Ok {
			coin = coinResp.Data.([]*model.Coin)[0]
		}
	}
	if coinResp.Status == core.APIStatus.Ok {
		coin = coinResp.Data.([]*model.Coin)[0]
	}

	c := colly.NewCollector()

	c.OnHTML("a[href='/chart/etherprice']", func(e *colly.HTMLElement) {
		//var priceText = strings.Split(e.DOM.Text(), " ")[0]
		//coin.PriceUsd = strings.Split(priceText, "$")[1]
		model.CoinDB.Update(nil, bson.M{"_id": coin.ID}, coin)
	})

	c.Visit("https://etherscan.io/")
	return &core.APIResponse{Status: core.APIStatus.Ok, Message: "Success"}
}

func GetCoinInfoDay() *core.APIResponse {
	version := GetCurrentVersion()

	c := colly.NewCollector()

	//c.OnRequest(func(request *colly.Request) {
	//	if request.URL.Path != "/tokens" {
	//		fmt.Println(request.URL.Path)
	//	}
	//})

	c.OnHTML("body", func(e *colly.HTMLElement) {
		if strings.HasPrefix(e.Request.URL.RequestURI(), "/dextracker_frame") {
			query := e.Request.URL.Query()

			coinCode := query["s"][0]
			fmt.Println(coinCode)
			e.DOM.Find("tr").EachWithBreak(func(i int, tr *goquery.Selection) bool {

				trade := model.Trade{
					Version:  version,
					CoinCode: coinCode,
				}

				tr.Find("td").EachWithBreak(func(tdIndex int, td *goquery.Selection) bool {
					switch tdIndex {
					case 0:
						trade.TxnHash = td.Text()
						// Check exists in db
						tradeResp := model.TradeDB.QueryOne(context.Background(), trade, nil)
						if tradeResp.Status == core.APIStatus.Ok {
							return false
						}
					case 1:
						t, _ := time.Parse("2006-01-02 15:04:05", td.Text())
						trade.Age = &t
					case 3:
						trade.Action = td.Text()
					case 4:
						trade.TokenAmountOut = ParseFloat(strings.Split(td.Text(), " ")[0], 0)
					case 5:
						trade.TokenAmountIn = ParseFloat(strings.Split(td.Text(), " ")[0], 0)
					case 6:
						trade.SwappedRate = ParseFloat(strings.Split(td.Text(), " ")[0], 0)
					case 7:
						if td.Text() != "N/A" {
							trade.TxnValue = ParseFloat(strings.Split(td.Text(), "$")[1], 0)
						}
						model.TradeDB.CreateOne(context.Background(), trade)
					}
					return true
				})
				return i < 30
			})
		}
	})

	c.OnHTML("tbody tr", func(e *colly.HTMLElement) {
		if e.Request.URL.RequestURI() == "/tokens" {
			coin := model.Coin{
				Version: version,
			}
			var coinResp *core.APIResponse
			e.ForEachWithBreak("td", func(tdIndex int, td *colly.HTMLElement) bool {
				switch tdIndex {
				case 1:
					// Handler Code & Name
					textCodeName := td.DOM.Find("a[href]").Text()
					codeName := strings.Split(textCodeName, " (")
					if len(codeName) >= 2 {
						coin.Name = codeName[0]
						coin.Code = strings.Replace(codeName[1], ")", "", 1)

						href, _ := td.DOM.Find("a[href]").Attr("href")
						token := strings.Replace(href, "/token/", "", 1)
						e.Request.Visit("/dextracker_frame?q=" + token + "&s=" + coin.Code + "&a=")

						// Check coin exists before
						coinQuery := model.Coin{
							Version: version,
							Code:    coin.Code,
						}
						coinResp = model.CoinDB.QueryOne(context.Background(), coinQuery, nil)
						if coinResp.Status == core.APIStatus.Ok {
							return false
						}
					}
				case 2:
					textPriceEth := td.ChildText(".d-block")
					coin.PriceEth = ParseFloat(strings.Split(textPriceEth, " Eth")[0], 0.0)

					textPriceBtc := strings.Split(td.ChildText(".small.text-secondary"), textPriceEth)
					coin.PriceBtc = ParseFloat(strings.Split(textPriceBtc[0], " Btc")[0], 0.0)

					textPriceUsd := strings.Split(td.Text, td.ChildText(".small.text-secondary"))
					coin.PriceUsd = ParseFloat(strings.Split(textPriceUsd[0], "$")[1], 0.0)
				case 3:
					coin.Change = ParseFloat(strings.Split(td.Text, "%")[0], 0.0)
				case 4:
					coin.Volume = ParseFloat(strings.Split(td.Text, "$")[1], 0.0)
				case 5:
					coin.MarketCap = ParseFloat(strings.Split(td.Text, "$")[1], 0.0)
				case 6:
					coin.Holders = ParseFloat(strings.Split(td.Text, " ")[0], 0.0)

					model.CoinDB.CreateOne(context.Background(), coin)
				}
				return true
			})
		}
	})

	//c.OnRequest(func(r *colly.Request) {
	//	fmt.Println("Visiting", r.URL)
	//})

	c.Visit("https://etherscan.io/tokens")

	return &core.APIResponse{Status: core.APIStatus.Ok, Message: "Success"}
}

func GetBlocks() *core.APIResponse {
	version := GetCurrentVersion()

	c := colly.NewCollector()
	c.OnHTML("tbody tr", func(e *colly.HTMLElement) {
		block := model.Block{
			Version: version,
		}
		e.ForEach("td", func(tdIndex int, td *colly.HTMLElement) {
			switch tdIndex {
			case 0:
				block.BlockCode = td.Text
			case 1:
				date, _ := time.Parse("2006-01-02 15:04:05", td.Text)
				block.Age = &date
			case 5:
				block.Miner = td.Text
			case 6:
				block.GasUsed = td.Text
			case 7:
				block.GasLimit = td.Text
			case 8:
				block.AvgGasPrice = td.Text
			case 9:
				block.Reward = td.Text
			}
		})
		model.BlockDB.CreateOne(context.Background(), block)
		//switch e.ChildText("td:first-child") {
		//
		//}
		//fmt.Println(e.Text)
	})

	c.Visit("https://etherscan.io/blocks")
	return &core.APIResponse{Status: core.APIStatus.Ok, Message: "Success"}
}
