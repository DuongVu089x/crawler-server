package action

import (
	"context"

	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

type AccountRegisterInput struct {
	UserName         string `json:"userName,omitempty"`
	FirstName        string `json:"firstName,omitempty"`
	LastName         string `json:"lastName,omitempty"`
	Email            string `json:"email,omitempty"`
	Password         string `json:"password,omitempty"`
	AllowExtraEmails bool   `json:"allowExtraEmails,omitempty"`
}

func AccountRegister(input *AccountRegisterInput) *core.APIResponse {

	account := model.Account{
		UserName:         input.UserName,
		FirstName:        input.FirstName,
		LastName:         input.LastName,
		Email:            input.Email,
		Password:         input.Password,
		AllowExtraEmails: input.AllowExtraEmails,
	}

	return model.AccountDB.CreateOne(context.Background(), account)
}

// CheckEmail...
func CheckEmail(email string) *core.APIResponse {
	accountQuery := model.Account{
		Email: email,
	}
	accountResp := model.AccountDB.QueryOne(context.Background(), accountQuery, nil)
	if accountResp.Status == core.APIStatus.Ok {
		account := accountResp.Data.([]*model.Account)[0]
		account.Password = ""
		return &core.APIResponse{Status: core.APIStatus.Ok, Message: "Email was regist before", Data: []*model.Account{account}}
	}
	return accountResp
}
