package action

import (
	"context"

	"gitlab.com/DuongVu089x/crawler-server/core"
	"gitlab.com/DuongVu089x/crawler-server/model"
)

type ListCoinInput struct {
	Coin      *model.Coin `json:"coin,omitempty"`
	SortField []string    `json:"sortField,omitempty"`
}

func ListCoins(input *ListCoinInput, offset, limit int, getTotal bool) *core.APIResponse {

	coinQuery := input.Coin
	coinQuery.Version = GetCurrentVersion()

	coinResp := model.CoinDB.Q(context.Background(), coinQuery, input.SortField, nil, offset, limit)
	if coinResp.Status == core.APIStatus.Ok && getTotal {
		totalResp := model.CoinDB.Count(context.Background(), coinQuery)
		if totalResp.Status == core.APIStatus.Ok {
			coinResp.Total = totalResp.Total
		}
	}
	return coinResp
}
